using System;
using System.Collections.Generic;


namespace AltF4Button {

    public class MessageBuilder {

        private string partlyMessage;
        private readonly Queue<string> messages;
        public char messageBeginMarker { get; }
        public char messageEndMarker { get; }
        public int messageCount => messages.Count;


        public MessageBuilder(char messageBeginMarker, char messageEndMarker) {
            this.messageBeginMarker = messageBeginMarker;
            this.messageEndMarker = messageEndMarker;
            messages = new Queue<string>();
            partlyMessage = null;
        }


        public void add(string data) {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            string message;
            bool messageStarted;

            if (partlyMessage != null) {
                message = partlyMessage;
                messageStarted = true;
                partlyMessage = null;
            }
            else {
                message = "";
                messageStarted = false;
            }

            foreach (var character in data) {
                if (messageStarted) {
                    if (character != messageEndMarker) {
                        message += character;
                    }
                    else {
                        messages.Enqueue(message);
                        message = "";
                        messageStarted = false;
                    }
                }
                else {
                    if (character == messageBeginMarker) {
                        messageStarted = true;
                    }
                }
            }

            if (messageStarted) partlyMessage = message;
        }


        public string getNextMessage() {
            return messages.Count > 0 ? messages.Dequeue() : null;
        }

    }

}