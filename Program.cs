﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;


namespace AltF4Button {

    internal static class Program {

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        private static void Main() {
            Thread.Sleep(5000);
            Console.WriteLine(getWindowTitle(GetForegroundWindow()));
            Thread.Sleep(1000);
            Console.WriteLine("Closing...");
            Console.WriteLine(getWindowProcessId(GetForegroundWindow()));
            Process.GetProcessById(getWindowProcessId(GetForegroundWindow())).Kill();
            var messageBuilder = new MessageBuilder('#', '%');
            var serialMessenger = new SerialMessenger("COM5", 115200, messageBuilder);
            serialMessenger.connect();
            serialMessenger.sendMessage("hello");
            serialMessenger.readMessages();
        }


        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();


        [DllImport("user32.dll")]
        private static extern int GetWindowText(IntPtr handle, StringBuilder text, int count);


        [DllImport("user32.dll")]
        private static extern IntPtr GetWindowThreadProcessId(IntPtr handle, out IntPtr processId);


        private static string getWindowTitle(IntPtr handle) {
            const int nChars = 256;
            var buff = new StringBuilder(nChars);
            return GetWindowText(handle, buff, nChars) > 0 ? buff.ToString() : null;
        }


        private static int getWindowProcessId(IntPtr handle) {
            GetWindowThreadProcessId(handle, out var pid);
            return (int) pid;
        }

    }

}