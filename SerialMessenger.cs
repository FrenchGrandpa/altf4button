using System;
using System.IO.Ports;


namespace AltF4Button {

    public class SerialMessenger {

        private readonly SerialPort serialPort;
        private readonly MessageBuilder messageBuilder;


        public SerialMessenger(string portName, int baudRate, MessageBuilder messageBuilder) {
            if (portName == null)
                throw new ArgumentNullException(nameof(portName));
            if (baudRate < 9600)
                throw new ArgumentOutOfRangeException(nameof(baudRate));

            serialPort = new SerialPort {
                BaudRate = baudRate,
                PortName = portName
            };
            this.messageBuilder = messageBuilder ?? throw new ArgumentNullException(nameof(messageBuilder));
        }


        public void connect() {
            if (serialPort.IsOpen) return;

            serialPort.Open();
            if (!serialPort.IsOpen) return;

            serialPort.DiscardInBuffer();
            serialPort.DiscardOutBuffer();
        }


        public bool sendMessage(string message) {
            if (!serialPort.IsOpen) return false;

            serialPort.Write(messageBuilder.messageBeginMarker + message + messageBuilder.messageEndMarker);
            return true;
        }


        public string[] readMessages() {
            if (!serialPort.IsOpen || serialPort.BytesToRead <= 0) return null;

            var data = serialPort.ReadExisting();
            messageBuilder.add(data);
            var messageCount = messageBuilder.messageCount;
            if (messageCount <= 0) return null;

            var messages = new string[messageCount];
            for (var i = 0; i < messageCount; i++)
                messages[i] = messageBuilder.getNextMessage();
            return messages;
        }

    }

}